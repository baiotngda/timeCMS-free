<div class="list-group">
    <a class="list-group-item active">控制面板</a>
    <a class="list-group-item" href="{{ url('admin') }}">系统统计</a>
    <a class="list-group-item" href="{{ url('admin/system') }}">系统设置</a>
    <a class="list-group-item" href="{{ url('admin/categories') }}">分类管理</a>
    <a class="list-group-item" href="{{ url('admin/articles') }}">文章管理</a>
    <a class="list-group-item" href="{{ url('admin/pages') }}">单页管理</a>
    <a class="list-group-item" href="{{ url('admin/users') }}">用户管理</a>
    <a class="list-group-item" href="{{ url('auth/logout') }}">退出</a>
</div>
