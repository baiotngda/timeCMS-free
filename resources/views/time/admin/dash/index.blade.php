@extends($theme.'.layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset($theme.'/css/admin.css') }}"/>
    <div class="container-fluid" id="main">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    @include($theme.'.admin.left')
                </div>
                <div class="col-sm-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">系统统计</div>
                        <div class="panel-body">
                            <p></p>
                            <div class="col-sm-6">
                                <a href="#" class="list-group-item active">最近五个用户<span class="badge">共{{ $user_num }}个</span></a>
                                @foreach($users as $user)
                                    <a href="#" class="list-group-item">{{ $user->name }}</a>
                                @endforeach
                            </div>
                            <div class="col-sm-6">
                                <a href="#" class="list-group-item active">最近五篇文章<span class="badge">共{{ $article_num }}篇</span></a>
                                @foreach($articles as $article)
                                    <a href="{{ url('article',$article->id) }}" class="list-group-item">{{ str_limit($article->title,32,'...') }}<span class="pull-right">{{ $article->updated_at->format('Y-m-d') }}</span></a>
                                @endforeach
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection